# Évaluation Git

- Prénom et nom : Thomas Lauze
- Commentaires : Bonjour

## Exercices

- 1 point - Fourcher et cloner le dépôt

- 1 point - Remplir votre nom ci-dessus 

- 1 point - Reprendre dans master le commit de la branche `equality_refinement` via `cherry-pick`
git cheery-pick fbca3b123bb5735d83f9b9efa8c415024bf4edd8

- 1 point - Défaire le commit `remove unused subtract functions`
git revert 

- 1 point - Fusionner dans `master` la branche `doc/intro`
git merge 12af7e2d2e21d1a61fa11fe63fb23f3cdff949d4

- 2 points - Fusionner dans `master` la branche `add_distance`

- 1 point - Pousser le nouveau `master`
git add -Av
git commit -m "Fusion doc/intro"
git push -u origin master

- 2 points - Créer une branche sur le dernier `master`, créer une nouvelle fonction de calcul, la committer, et pousser cette nouvelle branche 

- 2 points - Rebaser la branche `add_tests` sur `master`
- 1 point - Pousser la branche `add_tests` sur le dépôt distant
- 2 points - Ouvrir une merge request à partir de cette branche `add_tests` sur le dépôt source


## Questions

- Donner l’auteur du commit qui a introduit la fonction `add` dans ce projet : Père Noël

- Indiquer quelle commande utiliser, dans un projet Git,  pour changer le message du dernier commit git commit --amend -m "new_msg"

- Quelle commande, inverse de `git add`, permet de retirer un fichier du stage (prochain commit) ? git reset


- Donner le nom du créateur de Git : Jalil Arfaoui 

- Indiquer la(les) différence(s) entre les commandes `git init` et `git clone` : git init initialise un nouveau dossier et git clone copie un dossier existant en local
